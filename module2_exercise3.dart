void main() {
  // A) Use object to print the variables
  NakedInsurance nakedInsurance = NakedInsurance();

  String app = nakedInsurance.appName;
  String sector = nakedInsurance.sectorName;
  String developer = nakedInsurance.developerName;
  String year = nakedInsurance.yearWon;

  print("App of the Year: $app");
  print("Sector: $sector");
  print("Developer: $developer");
  print("Year: $year");

  print("");
  // B) Print app name with all caps
  nakedInsurance.appNameCapitalLetters();
}

// A) Create a class
class NakedInsurance {
  String appName = "Naked Insurance";
  String sectorName = "Finance";
  String developerName = "Sumarie Greybe";
  String yearWon = "2019";

// B) Create a function to tranforn
  void appNameCapitalLetters() {
    String allCapsName = appName.toUpperCase();

    print("Print in all caps: $allCapsName");
  } // close appNameCapitalLetters
} // close NakedInsurance 
