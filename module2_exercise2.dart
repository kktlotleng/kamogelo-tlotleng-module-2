void main() {
  // A list of winning apps of the MTN Business App of the Year Awards from 2012 - 2021
  final allWinners = <String>[
    "FNB Banking", //2012
    "SnapScan", //2013
    "Live Inspect", //2014
    "WumDrop", //2015
    "Domestly", //2016
    "Standard Bank Shyft", //2017
    "Khula", //2018
    "Naked Insurance", //2019
    "EasyEquities", //2020
    "Amabani" //2021
  ];

  // A) Sort and print all winning apps
  allWinners.sort();
  print("All winning apps (2012 - 2021): ");

  for (int i = 0; i < allWinners.length; i++) {
    print(allWinners[i]);
  }

  print("");

  // B) Print the winning apps of 2017 and 2018
  String winner2017 = allWinners[8];
  String winner2018 = allWinners[4];

  print("2017 winner: $winner2017");
  print("2018 winner: $winner2018");

  print("");

  // C) Get and print the total number of apps from the array
  int numberOfApps = allWinners.length;
  print("The total numbers of winners (2012 - 2021): $numberOfApps");
}
